﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using QlikLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Util
{
    public class Parameters
    {
        private static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        static string module = "Parameter", method = string.Empty;

        public static string getConfigParameters(string conParameter)
        {
            module = "Users"; method = "getConfigParameters";

            try
            {
                string path = ConfigurationManager.AppSettings["conSystemSetting"];

                System.Data.DataTable SlackDt = new System.Data.DataTable();
                SlackDt.Columns.Add("Name");
                SlackDt.Columns.Add("Key");
                SlackDt.Columns.Add("Value");

                System.Data.DataTable dialogflowDt = new System.Data.DataTable(); ;
                dialogflowDt.Columns.Add("Name");
                dialogflowDt.Columns.Add("Key");
                dialogflowDt.Columns.Add("Value");

                System.Data.DataTable qlikDt = new System.Data.DataTable(); ;
                qlikDt.Columns.Add("Name");
                qlikDt.Columns.Add("Key");
                qlikDt.Columns.Add("Value");

                if (File.Exists(path))
                {
                    StreamReader reader = new StreamReader
                    (
                        new FileStream(
                            path,
                            FileMode.Open,
                            FileAccess.Read,
                            FileShare.Read)
                    );
                    XmlDocument doc = new XmlDocument();
                    string xmlIn = reader.ReadToEnd();
                    reader.Close();
                    doc.LoadXml(xmlIn);
                    foreach (XmlNode child in doc.ChildNodes)
                        if (child.Name.Equals("AppSetting"))
                            foreach (XmlNode subNode in child.ChildNodes)
                                if (subNode.Name.Equals("add"))
                                    if (subNode.Attributes["key"].Value == conParameter)
                                        return Encryption.Decrypt(subNode.Attributes["value"].Value);

                    return string.Empty;
                }
                else
                {
                    log.Error(module, method, "Configuration file doesn't exist.", true);
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                log.Error(module, method, "Error while reading configuration parameter.", true);
                log.Error(module, method, ex.Message, true);
                log.Error(module, method, ex.StackTrace, true);
                return string.Empty;
            }
        }

        public static bool IsDimensionHasElementValue(string dimension, List<string> elements)
        {
            string conAppMetadataXlsx = ConfigurationManager.AppSettings["conAppMetadataXlsx"].ToString();

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(conAppMetadataXlsx, false))
            {
                string dimensionId = string.Empty;

                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();

                foreach (var sheet in sheets)
                {
                    if (sheet.Name == "Dimensions")
                    {
                        System.Data.DataTable dt = new System.Data.DataTable();
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(sheet.Id.Value);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        foreach (Cell cell in rows.ElementAt(0))
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));

                        foreach (Row row in rows)
                        {
                            DataRow dataRow = dt.NewRow();
                            for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                                dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));

                            if (dataRow["Name"].ToString().ToLower() == dimension.ToLower())
                            {
                                dimensionId = dataRow["Dim_ID"].ToString();
                                break;
                            }
                        }
                    }
                    else if (!string.IsNullOrEmpty(dimensionId) && sheet.Name == "Dimensions' Values")
                    {
                        System.Data.DataTable dt = new System.Data.DataTable();
                        WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(sheet.Id.Value);
                        Worksheet workSheet = worksheetPart.Worksheet;
                        SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        foreach (Cell cell in rows.ElementAt(0))
                            dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));

                        foreach (Row row in rows)
                        {
                            DataRow dataRow = dt.NewRow();
                            for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                                dataRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));

                            if (dataRow["Dim_ID"].ToString() == dimensionId)
                                dt.Rows.Add(dataRow);
                        }

                        //dt.Rows.RemoveAt(0);

                        foreach (string element in elements)
                        {
                            var dataRows = dt.Select("Value = '" + element + "'");

                            if (dataRows.Count() > 0)
                                return true;
                        }
                    }
                }
            }

            return false;
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerText.ToString();

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }
    }
}
