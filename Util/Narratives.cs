﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{

    public class Narratives
    {
        public Measure[] measures { get; set; }
        public Dimension[] dimensions { get; set; }
        public Datavalue[] dataValues { get; set; }
        public string apiKey { get; set; }
        public Extra extra { get; set; }
    }

    public class Extra
    {
        public int verbosity { get; set; }
        public string dataType { get; set; }
        public string sheetTitle { get; set; }
        public string objectType { get; set; }
        public bool hasSelection { get; set; }
        public string responseForm { get; set; }
    }

    public class Measure
    {
        public string value { get; set; }
        public int id { get; set; }
        public string cumulative { get; set; }
        public string largeValue { get; set; }
        public string measureIn { get; set; }
    }

    public class Dimension
    {
        public string value { get; set; }
        public int id { get; set; }
        public string singular { get; set; }
        public string plural { get; set; }
    }

    public class Datavalue
    {
        public Dim[] dim { get; set; }
        public Mea[] mea { get; set; }
    }

    public class Dim
    {
        public string value { get; set; }
        public int id { get; set; }
    }

    public class Mea
    {
        public string value { get; set; }
        public int id { get; set; }
    }

}
