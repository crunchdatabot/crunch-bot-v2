﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Cognitive.LUIS;
using QlikLog;
using ApiAiSDK;
using System.Configuration;

namespace Dialogflow
{
    public class LUIS
    {
        private LuisClient luisClient = null;
        public bool IsConnected = false;

        private static LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);
        string module = "LUIS", method = string.Empty;

        public LUIS(string appID, string subscriptionKey, string baseApiUrl)
        {
            luisClient = new LuisClient(appID, subscriptionKey, baseApiUrl);

            if (luisClient != null)
                IsConnected = true;
        }

        private Intent Predicted = new Intent();

        public bool HasPrediction { get { return Predicted.HasPrediction; } }
        public string OriginalQuery { get { return Predicted.OriginalQuery; } }
        public IntentType IntentType { get { return Predicted.IntentType; } set { Predicted.IntentType = value; } }
        public string Measure { get { return Predicted.Measure; } set { Predicted.Measure = value; } }
        public string Measure2 { get { return Predicted.Measure2; } set { Predicted.Measure2 = value; } }
        public List<string> Elements { get { return Predicted.Elements; } set { Predicted.Elements = value; } }
        public string Dimension { get { return Predicted.Dimension; } set { Predicted.Dimension = value; } }
        public string Percentage { get { return Predicted.Percentage; } set { Predicted.Percentage = value; } }
        public List<double?> Number { get { return Predicted.Number; } set { Predicted.Number = value; } }
        public string ChartType { get { return Predicted.ChartType; } set { Predicted.ChartType = value; } }
        public double DistanceKm { get { return Predicted.DistanceKm; } set { Predicted.DistanceKm = value; } }
        public string Language { get { return Predicted.Language; } set { Predicted.Language = value; } }
        public string Response { get { return Predicted.Response; } set { Predicted.Response = value; } }
        public List<string> Date { get { return Predicted.Date; } set { Predicted.Date = value; } }
        public List<string> DatePeriod { get { return Predicted.DatePeriod; } set { Predicted.DatePeriod = value; } }
        public List<string> DatePeriod1 { get { return Predicted.DatePeriod1; } set { Predicted.DatePeriod1 = value; } }
        public List<string> OriginalDatePeriod { get { return Predicted.OriginalDatePeriod; } set { Predicted.OriginalDatePeriod = value; } }

        public async Task<bool> Predict(string textToPredict)
        {
            Predicted.HasPrediction = false;
            Predicted.OriginalQuery = null;
            Predicted.IntentType = IntentType.None;
            Predicted.Response = null;
            Predicted.Measure = null;
            Predicted.Measure2 = null;
            Predicted.Elements = new List<string>();
            Predicted.Dimension = null;
            Predicted.Percentage = null;
            Predicted.Number = new List<double?>();
            Predicted.ChartType = null;
            Predicted.Date = new List<string>();
            Predicted.DatePeriod = new List<string>();
            Predicted.DatePeriod1 = new List<string>();
            Predicted.OriginalDatePeriod = new List<string>();

            //Predicted.DistanceKm = 0;
            //Predicted.Language = null;

            try
            {
                LuisResult luisResult = await luisClient.Predict(textToPredict);

                if (luisResult.TopScoringIntent.Name == "Greetings" ||
                    luisResult.TopScoringIntent.Name == "Hello" ||
                    luisResult.TopScoringIntent.Name == "Welcome" ||
                    luisResult.TopScoringIntent.Name == "Disgrace" ||
                    luisResult.TopScoringIntent.Name == "GetStarted" ||
                    luisResult.TopScoringIntent.Name == "Gratitude" ||
                    luisResult.TopScoringIntent.Name == "Quit")
                {
                    Predicted.HasPrediction = true;
                    Predicted.OriginalQuery = textToPredict;
                    if (luisResult.TopScoringIntent.Name == "Welcome")
                        Predicted.IntentType = IntentType.Welcome;
                    else if (luisResult.TopScoringIntent.Name == "GetStarted")
                        Predicted.IntentType = IntentType.GetStarted;
                    else if (luisResult.TopScoringIntent.Name == "Gratitude")
                        Predicted.IntentType = IntentType.Gratitude;
                    else if (luisResult.TopScoringIntent.Name == "Greetings")
                        Predicted.IntentType = IntentType.Greetings;
                    else
                        Predicted.IntentType = IntentType.DirectResponse;
                    Predicted.Response = textToPredict;
                }
                else
                {
                    if (luisResult.Entities.Count != 0)
                    {
                        List<Entity> entityList = luisResult.GetAllEntities();
                        foreach (Entity entity in entityList)
                        {
                            if (entity.Name == "Measure")
                            {
                                if (string.IsNullOrEmpty(Predicted.Measure))
                                    Predicted.Measure = (((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First).ToString();
                                else
                                    Predicted.Measure2 = (((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First).ToString();
                            }
                            else if (entity.Name == "Dimension")
                                Predicted.Dimension = (((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First).ToString();
                            else if (entity.Name.StartsWith("dim"))
                            {
                                if (!Predicted.Elements.Contains((((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First).ToString()))
                                    Predicted.Elements.Add((((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First).ToString());
                            }
                            else if (entity.Name == "builtin.datetimeV2.daterange")
                            {
                                Newtonsoft.Json.Linq.JToken jToken = ((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First;
                                if (Predicted.DatePeriod.Count == 0)
                                {
                                    Predicted.DatePeriod.Add(jToken["start"].ToString());
                                    Predicted.DatePeriod.Add(jToken["end"].ToString());
                                    Predicted.OriginalDatePeriod = new List<string>();
                                }
                                else
                                {
                                    Predicted.DatePeriod1.Add(jToken["start"].ToString());
                                    Predicted.DatePeriod1.Add(jToken["end"].ToString());
                                }
                                if (Predicted.OriginalDatePeriod.Count == 0)
                                    Predicted.OriginalDatePeriod = new List<string>();
                                Predicted.OriginalDatePeriod.Add(entity.Value);
                            }
                            else if (entity.Name == "builtin.datetimeV2.date")
                            {
                                Newtonsoft.Json.Linq.JToken jToken = ((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First;
                                if (Predicted.Date.Count == 0)
                                    Predicted.Date = new List<string>();
                                Predicted.Date.Add(jToken["value"].ToString());
                            }
                            else if (entity.Name == "ChartType")
                                Predicted.ChartType = (((Newtonsoft.Json.Linq.JArray)entity.Resolution["values"]).First).ToString();

                            else if (entity.Name == "builtin.percentage")
                            {
                                Predicted.Percentage = entity.Value;
                                Predicted.Number = PercentageToDouble(Predicted.Percentage);
                            }
                            else if (entity.Name == "builtin.number")
                            {
                                Predicted.Number.Add(Convert.ToDouble(entity.Value));
                                //Predicted.Elements.Add(entity.Value);
                            }
                        }

                    }

                    if (luisResult.TopScoringIntent.Name == "input.unknown")
                    {
                        this.Predicted.HasPrediction = false;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.None;
                        this.Predicted.Response = "I do not understand you, could you try again with other question?";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowMeasure")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.KPI;
                        this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowChart")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.Chart;
                        this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowMeasureForDimension" || luisResult.TopScoringIntent.Name == "ShowMeasureForElement")
                    {
                        if (string.IsNullOrEmpty(Predicted.Dimension) && Predicted.Elements.Count() == 0)
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = textToPredict;
                            this.Predicted.IntentType = IntentType.KPI;
                            this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                        }
                        else
                        {
                            this.Predicted.HasPrediction = true;
                            this.Predicted.OriginalQuery = textToPredict;
                            this.Predicted.IntentType = IntentType.Measure4Element;
                            this.Predicted.Response = "You want to know the value of " + Predicted.Measure;
                        }
                    }
                    else if (luisResult.TopScoringIntent.Name == "Alert")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.Alert;
                        this.Predicted.Response = "You want to be alerted when " + Predicted.Measure + " changes by " + Predicted.Percentage;
                    }
                    else if (luisResult.TopScoringIntent.Name == "Ranking") //Check if the Intent was of ranking Type
                    {
                        if (luisResult.Entities.ContainsKey("Superlatives"))
                        {
                            string RankingIntentType = null;
                            foreach (var superlativeValue in luisResult.Entities["Superlatives"][0].Resolution.Values)
                            {
                                var rankingintent = (Newtonsoft.Json.Linq.JArray)superlativeValue;
                                RankingIntentType = ((Newtonsoft.Json.Linq.JValue)rankingintent.First).Value.ToString();
                            }

                            if (RankingIntentType == "top")
                            {
                                this.Predicted.HasPrediction = true;
                                this.Predicted.OriginalQuery = textToPredict;
                                this.Predicted.IntentType = IntentType.RankingTop;
                                this.Predicted.Response = "You want to know the top elements by " + Predicted.Dimension;
                            }
                            else if (RankingIntentType == "bottom")
                            {
                                this.Predicted.HasPrediction = true;
                                this.Predicted.OriginalQuery = textToPredict;
                                this.Predicted.IntentType = IntentType.RankingBottom;
                                this.Predicted.Response = "You want to know the bottom elements by " + Predicted.Dimension;
                            }
                        }
                    }
                    else if (luisResult.TopScoringIntent.Name == "ApplyFilters" || luisResult.TopScoringIntent.Name == "Filter")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.Filter;
                        this.Predicted.Response = "You want to filter " + Predicted.Dimension + " by " + Predicted.Elements;
                    }
                    else if (luisResult.TopScoringIntent.Name == "Hello" || luisResult.TopScoringIntent.Name == "input.welcome")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.Hello;
                        this.Predicted.Response = "Hello";
                    }
                    else if (luisResult.TopScoringIntent.Name == "Reports")
                    {
                        this.Predicted.HasPrediction = false;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.None;
                        this.Predicted.Response = "I do not have the logic to answer " + textToPredict + " yet";

                        ////Temporary commented
                        //this.Predicted.HasPrediction = true;
                        //this.Predicted.OriginalQuery = TextToPredict;
                        //this.Predicted.IntentType = IntentType.Reports;
                        //this.Predicted.Response = "I will show you the available reports";
                    }
                    else if (luisResult.TopScoringIntent.Name == "CreateChart")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.CreateChart;
                        this.Predicted.Response = "I will create a chart for you";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowAnalysis")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowAnalysis;
                        this.Predicted.Response = "I will create an analysis for you";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ContactQlik")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ContactQlik;
                        this.Predicted.Response = "You can go to www.qlik.com";
                    }
                    else if (luisResult.TopScoringIntent.Name == "Help")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.Help;
                        this.Predicted.Response = "You can ask for any information in the current Qlik Sense app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ClearFilters" || luisResult.TopScoringIntent.Name == "ClearAllFilters")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ClearAllFilters;
                        this.Predicted.Response = "I will clear all filters";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ClearDimensionFilter")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ClearDimensionFilter;
                        this.Predicted.Response = "I will clear this dimension filter";
                    }
                    else if (luisResult.TopScoringIntent.Name == "CurrentSelections")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.CurrentSelections;
                        this.Predicted.Response = "I will show you the current selections in the app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowApps" || luisResult.TopScoringIntent.Name == "ShowAllApps")
                    {
                        this.Predicted.HasPrediction = false;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.None;
                        this.Predicted.Response = "I do not have the logic to answer " + textToPredict + " yet";

                        ////Temporary commented
                        //this.Predicted.HasPrediction = true;
                        //this.Predicted.OriginalQuery = TextToPredict;
                        //this.Predicted.IntentType = IntentType.ShowAllApps;
                        //this.Predicted.Response = "I will show all the available apps you can connect";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowVisualizations")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowAllVisualizations;
                        this.Predicted.Response = "I will show all the available visualizations you can connect";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowDimensions" || luisResult.TopScoringIntent.Name == "ShowAllDimensions")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowAllDimensions;
                        this.Predicted.Response = "I will show you all the master dimensions in the current app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowMeasures" || luisResult.TopScoringIntent.Name == "ShowAllMeasures")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowAllMeasures;
                        this.Predicted.Response = "I will show you all the master measures in the current app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowSheets" || luisResult.TopScoringIntent.Name == "ShowAllSheets")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowAllSheets;
                        this.Predicted.Response = "I will show you all the sheets in the current app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowStories" || luisResult.TopScoringIntent.Name == "ShowAllStories")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowAllStories;
                        this.Predicted.Response = "I will show you all the stories in the current app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowKPIs")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowKPIs;
                        this.Predicted.Response = "I will show you the most used metrics in the current app";
                    }
                    else if (luisResult.TopScoringIntent.Name == "MeasureVsMeasure" || luisResult.TopScoringIntent.Name == "ShowMeasureByMeasure")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowMeasureByMeasure;
                        this.Predicted.Response = "I will show you the result of analyzing these two measures";
                    }
                    else if (luisResult.TopScoringIntent.Name == "ShowListOfElements")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.ShowListOfElements;
                        this.Predicted.Response = "I will show you a list of elements for this dimension";
                    }
                    else if (luisResult.TopScoringIntent.Name == "Apologize")
                    {
                        this.Predicted.HasPrediction = true;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.Apologize;
                        this.Predicted.Response = "OK";
                    }
                    else if (luisResult.TopScoringIntent.Name == "Range")
                    {
                        if (luisResult.Entities.ContainsKey("Comparator"))
                        {
                            string ComparatorType = null;
                            foreach (var superlativeValue in luisResult.Entities["Comparator"][0].Resolution.Values)
                            {
                                var ComparatorEntity = (Newtonsoft.Json.Linq.JArray)superlativeValue;
                                ComparatorType = ((Newtonsoft.Json.Linq.JValue)ComparatorEntity.First).Value.ToString();
                            }
                            if (ComparatorType == ">" || ComparatorType == ">=")
                            {
                                this.Predicted.HasPrediction = true;
                                this.Predicted.OriginalQuery = textToPredict;
                                this.Predicted.IntentType = IntentType.ShowElementsAboveValue;
                                this.Predicted.Response = "You want to know the elements that meet a measure is above this value";
                            }
                            else if (ComparatorType == "<" || ComparatorType == "<=")
                            {
                                this.Predicted.HasPrediction = true;
                                this.Predicted.OriginalQuery = textToPredict;
                                this.Predicted.IntentType = IntentType.ShowElementsBelowValue;
                                this.Predicted.Response = "You want to know the elements that meet a measure is below this value";
                            }
                            else if (ComparatorType == "between")
                            {
                                this.Predicted.HasPrediction = true;
                                this.Predicted.OriginalQuery = textToPredict;
                                this.Predicted.IntentType = IntentType.ShowElementsBetweenValues;
                                this.Predicted.Response = "You want to know the elements that meet a measure is between values";
                            }
                            else if (ComparatorType == "==")
                            { }
                            else if (ComparatorType == "!=")
                            { }
                        }
                    }
                    else
                    {
                        this.Predicted.HasPrediction = false;
                        this.Predicted.OriginalQuery = textToPredict;
                        this.Predicted.IntentType = IntentType.None;
                        this.Predicted.Response = "I do not have the logic to answer " + textToPredict + " yet";
                    }
                    //else if (response.Result.Action == "GoodAnswer")
                    //{
                    //    this.Predicted.HasPrediction = true;
                    //    this.Predicted.OriginalQuery = TextToPredict;
                    //    this.Predicted.IntentType = IntentType.GoodAnswer;
                    //    this.Predicted.Response = ":-)";
                    //}
                    //else if (response.Result.Action == "Bye")
                    //{
                    //    this.Predicted.HasPrediction = true;
                    //    this.Predicted.OriginalQuery = TextToPredict;
                    //    this.Predicted.IntentType = IntentType.Bye;
                    //    this.Predicted.Response = "Bye";
                    //}
                    //else if (response.Result.Action == "BadWords" )
                    //{
                    //    this.Predicted.HasPrediction = true;
                    //    this.Predicted.OriginalQuery = TextToPredict;
                    //    this.Predicted.IntentType = IntentType.BadWords;
                    //    this.Predicted.Response = ":-(\nI prefer not to answer this type of questions, I am a robot but I am very polite.";
                    //}
                }
                //else if (luisResult.TopScoringIntent.Name == "Followup" || luisResult.TopScoringIntent.Name == "FollowupNext")
                //{
                //    this.Predicted.HasPrediction = true;
                //    this.Predicted.OriginalQuery = TextToPredict;

                //    var context = response.Result.Contexts.Where(x => x.Name == "followup").ToList();

                //    foreach (System.Collections.Generic.KeyValuePair<string, object> param in context[0].Parameters)
                //    {
                //        string strParam = (string)param.Value.ToString();

                //        if (strParam.StartsWith("[\r\n"))
                //            strParam = (string)param.Value.ToString().Replace("[\r\n  \"", "").Replace("\"\r\n]", "");

                //        string comparator = param.Key.ToString().StartsWith("next") ? "next-" : param.Key.ToString().StartsWith("followup") ? "followup-" : string.Empty;

                //        if (!string.IsNullOrEmpty(strParam) && !param.Key.ToString().EndsWith(".original"))
                //        {
                //            if (param.Key == "Measure") Predicted.Measure = strParam;
                //            else if (param.Key == "Measure1") Predicted.Measure2 = strParam;
                //            else if (param.Key == "Dimension") Predicted.Dimension = strParam;
                //            else if (param.Key.StartsWith(comparator + "dim")) { if (!Predicted.Element.Contains(strParam)) Predicted.Element.Add(strParam); }
                //            else if (param.Key == comparator + "Date-Period") Predicted.DatePeriod = strParam.Split('/').ToList();
                //            else if (param.Key == comparator + "Date-Period1") Predicted.DatePeriod1 = strParam.Split('/').ToList();
                //            else if (param.Key == comparator + "OriginalDate-Period") { Predicted.OriginalDatePeriod = new List<string>(); Predicted.OriginalDatePeriod.Add(strParam); }
                //            else if (param.Key == comparator + "OriginalDate-Period1") { Predicted.OriginalDatePeriod[1] = strParam; }
                //            else if ((param.Key == comparator + "Date" || param.Key == comparator + "Date1")) { if (!Predicted.Date.Contains(strParam)) Predicted.Date.Add(strParam); }
                //            else if (param.Key == "ChartType") Predicted.ChartType = strParam;
                //            else if (param.Key == "Intent")
                //            {
                //                switch (strParam)
                //                {
                //                    case "ShowMeasure":
                //                        this.Predicted.IntentType = IntentType.KPI;
                //                        break;
                //                    case "ShowMeasureForDimension":
                //                    case "ShowMeasureForElement":
                //                        this.Predicted.IntentType = IntentType.Measure4Element;
                //                        break;
                //                    case "Ranking":
                //                        if (!string.IsNullOrEmpty(response.Result.Parameters["Superlatives"].ToString()))
                //                            if (response.Result.Parameters["Superlatives"].ToString() == "top")
                //                                this.Predicted.IntentType = IntentType.RankingTop;
                //                            else if (response.Result.Parameters["Superlatives"].ToString() == "bottom")
                //                                this.Predicted.IntentType = IntentType.RankingBottom;
                //                        break;
                //                    case "CreateChart":
                //                        this.Predicted.IntentType = IntentType.CreateChart;
                //                        break;
                //                    case "MeasureVsMeasure":
                //                    case "ShowMeasureByMeasure":
                //                        this.Predicted.IntentType = IntentType.ShowMeasureByMeasure;
                //                        break;
                //                    case "Range":
                //                        if (!string.IsNullOrEmpty(response.Result.Parameters["Comparator"].ToString()))
                //                        {
                //                            if (response.Result.Parameters["Comparator"].ToString() == ">" || response.Result.Parameters["Comparator"].ToString() == ">=")
                //                                this.Predicted.IntentType = IntentType.ShowElementsAboveValue;
                //                            else if (response.Result.Parameters["Comparator"].ToString() == "<" || response.Result.Parameters["Comparator"].ToString() == "<=")
                //                                this.Predicted.IntentType = IntentType.ShowElementsBelowValue;
                //                            else if (response.Result.Parameters["Comparator"].ToString() == "==")
                //                            { }
                //                            else if (response.Result.Parameters["Comparator"].ToString() == "!=")
                //                            { }
                //                        }
                //                        break;
                //                }
                //            }
                //        }
                //    }

                //    if (luisResult.TopScoringIntent.Name == "FollowupNext")
                //    {
                //        var request = (HttpWebRequest)WebRequest.Create("https://api.dialogflow.com/v1/contexts?sessionId=" + response.SessionId);
                //        request.Method = "DELETE";
                //        request.Headers.Add("Authorization", "Bearer " + Parameters.getConfigParameters("conDialogflowKey").ToString());
                //        var resp = (HttpWebResponse)request.GetResponse();
                //        var responseString = new StreamReader(resp.GetResponseStream()).ReadToEnd();
                //    }
                //}
            }

            catch (Exception e)
            {
                method = "Predict";
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
                Predicted.HasPrediction = false;
            }

            return Predicted.HasPrediction;
        }

        private List<double?> PercentageToDouble(string Percentage)
        {
            List<double?> d = new List<double?>();
            string strPct = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.PercentSymbol;

            if (!Percentage.Contains(strPct)) return null;

            try
            {
                d.Add(double.Parse(Percentage.Replace(strPct, "")) / 100);
            }
            catch (Exception e)
            {
                d = null;
            }

            return d;
        }
    }
}
