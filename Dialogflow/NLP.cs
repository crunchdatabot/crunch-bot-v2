﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dialogflow
{
    public class Intent
    {
        public bool HasPrediction { get; set; }
        public string OriginalQuery { get; set; }
        public IntentType IntentType { get; set; }
        public string Measure { get; set; }
        public string Measure2 { get; set; }
        public List<string> Elements { get; set; }
        public string Dimension { get; set; }
        public string Dimension2 { get; set; }
        public string Percentage { get; set; }
        public List<double?> Number { get; set; }
        public string ChartType { get; set; }
        public double DistanceKm { get; set; }
        public string Language { get; set; }
        public string Response { get; set; }
        public List<string> Date { get; set; }
        public List<string> DatePeriod { get; set; }
        public List<string> DatePeriod1 { get; set; }
        public List<string> OriginalDatePeriod { get; set; }
    }

    public enum IntentType
    {
        None = 0, KPI = 1, Chart = 2, Measure4Element = 3, Alert = 4, GoodAnswer = 5, RankingTop = 6, RankingBottom = 7, Hello = 8, Bye = 9, Filter = 10, Reports = 11,
        BadWords = 12, CreateChart = 13, ShowAnalysis = 14, ContactQlik = 15, Help = 16, ClearAllFilters = 17, ClearDimensionFilter = 18, CurrentSelections = 19,
        ShowAllApps = 20, ShowAllDimensions = 21, ShowAllMeasures = 22, ShowAllSheets = 23, ShowAllStories = 24, ShowKPIs = 25, ShowMeasureByMeasure = 26,
        ShowListOfElements = 27, Apologize = 28, DirectResponse = 29, ShowElementsAboveValue = 30, ShowElementsBelowValue = 31, ShowAllVisualizations = 32,
        ShowElementsBetweenValues = 33, Welcome = 34, GetStarted = 35, Gratitude = 36, Greetings = 37
    }

    public class NLP
    {
        Dialogflow QlikDialogflow;
        LUIS QlikLuis;

        public void StartDialogflow(string AppID, string Language = "English")
        {
            QlikDialogflow = new Dialogflow(AppID, Language);
        }

        public void StartLUIS(string appID, string subscriptionKey, string baseApiUrl)
        {
            QlikLuis = new LUIS(appID, subscriptionKey, baseApiUrl);
        }

        private Intent Predicted = new Intent();

        public bool HasPrediction { get { return Predicted.HasPrediction; } }
        public string OriginalQuery { get { return Predicted.OriginalQuery; } }
        public IntentType IntentType { get { return Predicted.IntentType; } set { Predicted.IntentType = value; } }
        public string Measure { get { return Predicted.Measure; } set { Predicted.Measure = value; } }
        public string Measure2 { get { return Predicted.Measure2; } set { Predicted.Measure2 = value; } }
        public List<string> Elements { get { return Predicted.Elements; } set { Predicted.Elements = value; } }
        public string Dimension { get { return Predicted.Dimension; } set { Predicted.Dimension = value; } }
        public string Dimension2 { get { return Predicted.Dimension2; } set { Predicted.Dimension2 = value; } }
        public string Percentage { get { return Predicted.Percentage; } set { Predicted.Percentage = value; } }
        public List<double?> Number { get { return Predicted.Number; } set { Predicted.Number = value; } }
        public string ChartType { get { return Predicted.ChartType; } set { Predicted.ChartType = value; } }
        public double DistanceKm { get { return Predicted.DistanceKm; } set { Predicted.DistanceKm = value; } }
        public string Language { get { return Predicted.Language; } set { Predicted.Language = value; } }
        public string Response { get { return Predicted.Response; } set { Predicted.Response = value; } }
        public List<string> Date { get { return Predicted.Date; } set { Predicted.Date = value; } }
        public List<string> DatePeriod { get { return Predicted.DatePeriod; } set { Predicted.DatePeriod = value; } }
        public List<string> DatePeriod1 { get { return Predicted.DatePeriod1; } set { Predicted.DatePeriod1 = value; } }
        public List<string> OriginalDatePeriod { get { return Predicted.OriginalDatePeriod; } set { Predicted.OriginalDatePeriod = value; } }

        public Intent GetPredictedIntent()
        {
            return Predicted;
        }

        public Intent GetCopyOfPredictedIntent()
        {
            NLP n = new NLP();
            n.Predicted.HasPrediction = this.HasPrediction;
            n.Predicted.OriginalQuery = this.OriginalQuery;
            n.Predicted.IntentType = this.IntentType;
            n.Predicted.Measure = this.Measure;
            n.Predicted.Measure2 = this.Measure2;
            n.Predicted.Elements = this.Elements;
            n.Predicted.Dimension = this.Dimension;
            n.Predicted.Dimension2 = this.Dimension2;
            n.Predicted.Percentage = this.Percentage;
            n.Predicted.Number = this.Number;
            n.Predicted.ChartType = this.ChartType;
            n.Predicted.DistanceKm = this.DistanceKm;
            n.Predicted.Language = this.Language;
            n.Predicted.Response = this.Response;
            n.Predicted.Date = this.Date;
            n.Predicted.DatePeriod = this.DatePeriod;
            n.Predicted.DatePeriod1 = this.DatePeriod1;
            n.Predicted.OriginalDatePeriod = this.OriginalDatePeriod;

            return n.Predicted;
        }

        public async Task<bool> Predict(string TextToPredict, bool isDialogflow = true)
        {
            bool result = false;

            Predicted.Measure = null;
            Predicted.Measure2 = null;
            Predicted.Elements = null;
            Predicted.Dimension = null;
            Predicted.Dimension2 = null;
            Predicted.ChartType = null;
            Predicted.Date = null;
            Predicted.DatePeriod = null;
            Predicted.DatePeriod1 = null;
            Predicted.OriginalDatePeriod = null;

            Predicted.Percentage = null;
            Predicted.DistanceKm = 0;
            Predicted.Language = null;

            if (isDialogflow)
            {
                result = QlikDialogflow.Predict(TextToPredict);

                assignPrediction(QlikDialogflow);
            }
            else
            {
                result = await QlikLuis.Predict(TextToPredict);

                assignPrediction(QlikLuis);
            }

            return result;
        }

        private void assignPrediction(dynamic nlpResult)
        {
            Predicted.HasPrediction = nlpResult.HasPrediction;
            Predicted.OriginalQuery = nlpResult.OriginalQuery;
            Predicted.IntentType = nlpResult.IntentType;
            Predicted.Measure = nlpResult.Measure;
            Predicted.Measure2 = nlpResult.Measure2;
            Predicted.Elements = nlpResult.Elements;
            Predicted.Dimension = nlpResult.Dimension;
            Predicted.Dimension2 = nlpResult.Dimension2;
            Predicted.Percentage = nlpResult.Percentage;
            Predicted.Number = nlpResult.Number;
            Predicted.ChartType = nlpResult.ChartType;
            Predicted.DistanceKm = nlpResult.DistanceKm;
            Predicted.Language = nlpResult.Language;
            Predicted.Response = nlpResult.Response;
            Predicted.Date = nlpResult.Date;
            Predicted.DatePeriod = nlpResult.DatePeriod;
            Predicted.DatePeriod1 = nlpResult.DatePeriod1;
            Predicted.OriginalDatePeriod = nlpResult.OriginalDatePeriod;
        }
    }
}