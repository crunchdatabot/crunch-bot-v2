﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slack
{
    public partial class Chat
    {
        /// <summary>
        /// Class post image arguments
        /// </summary>
        public class PostImageArguments
        {
            public string title { get; set; }
            public string initial_comment { get; set; }
            public string channel { get; set; }
            public string filename { get; set; }
            public string file { get; set; }
        }

        /// <summary>
        /// Class post attachement message arguments
        /// </summary>
        public class PostAttachementMessageArguments
        {
            public struct Button
            {
                public string text { get; set; }
                public string channel { get; set; }
                public Attachment[] attachments { get; set; }
            }

            public struct Attachment
            {
                public string text { get; set; }
                public string fallback { get; set; }
                public string callback_id { get; set; }
                public string color { get; set; }
                public string attachment_type { get; set; }
                public Action[] actions { get; set; }
            }

            public struct Action
            {
                public string type { get; set; }
                public string name { get; set; }
                public string text { get; set; }
                public string value { get; set; }
                public string style { get; set; }
                //public string url { get; set; }
            }

            public String channel = "";
            public String text = "";
            public String username = "";
            public Boolean as_user = false;
            public String parse = "full";
            public Int32 link_names = 1;
            public Attachment[] attachments = null;
            public Boolean unfurl_links = true;
            public Boolean unfurl_media = true;
            public String icon_url = "";
            public String icon_emoji = "";
        }


        /// <summary>
        /// Class post message arguments
        /// </summary>
        public class PostMessageArguments
        {
            public struct attachment
            {
                public String type { get; set; }
                public String value { get; set; }
                public String fallback { get; set; }
                public String title { get; set; }
                public String title_link { get; set; }
                public String text { get; set; }
                public String image_url { get; set; }
                public String color { get; set; }
            }

            public String channel = "";
            public String text = "";
            public String username = "";
            public Boolean as_user = false;
            public String parse = "full";
            public Int32 link_names = 1;
            public attachment[] attachments = null;
            public Boolean unfurl_links = true;
            public Boolean unfurl_media = true;
            public String icon_url = "";
            public String icon_emoji = "";
        }
    }
}
