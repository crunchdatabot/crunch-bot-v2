﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Slack
{


    public partial class Chat
    {
        private Slack.Client _client;

        public Chat(Slack.Client Client)
        {
            _client = Client;
        }

        public Boolean Delete(String channel, Slack.TimeStamp ts)
        {
            //https://api.slack.com/methods/chat.delete
            dynamic Response;
            try
            {
                String strResponse = _client.APIRequest(
                    "https://slack.com/api/chat.delete?token=" + _client.APIKey +
                    "&channel=" + System.Web.HttpUtility.UrlEncode(channel) +
                    "&ts=" + System.Web.HttpUtility.UrlEncode(ts.ToString()));
                Response = System.Web.Helpers.Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not delte message.", ex);
            }
            _client.CheckForError(Response);
            return Response.ok;
        }

        public UpdateMessageResponse Update(UpdateMessageArguments args)
        {
            //https://api.slack.com/methods/chat.update
            String strURL = "";
            try
            {
                strURL =
                    "https://slack.com/api/chat.update?token=" + _client.APIKey +
                    "&channel=" + System.Web.HttpUtility.UrlEncode(args.channel) +
                    "&ts=" + System.Web.HttpUtility.UrlEncode(args.ts.ToString()) +
                    "&text=" + System.Web.HttpUtility.UrlEncode(args.text) +
                    "&parse=" + System.Web.HttpUtility.UrlEncode(args.parse) +
                    "&link_names=" + System.Web.HttpUtility.UrlEncode(args.link_names.ToString());
                if (args.attachments != null)
                {
                    strURL += "&attachments=[{";
                    for (Int32 intAttachment = 0; intAttachment < args.attachments.Length; intAttachment++)
                    {
                        strURL +=
                            "\"" + System.Web.HttpUtility.UrlEncode(args.attachments[intAttachment].type) + "\": " +
                            "\"" + System.Web.HttpUtility.UrlEncode(args.attachments[intAttachment].value) + "\"";
                        if (intAttachment < (args.attachments.Length - 1))
                        {
                            strURL += ", ";
                        }
                    }
                    strURL += "}]";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Could not update chat message.", ex);
            }
            dynamic Response;
            try
            {
                String strResponse = _client.APIRequest(strURL);
                Response = System.Web.Helpers.Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not update chat message.", ex);
            }
            _client.CheckForError(Response);
            return new UpdateMessageResponse(Response);
        }

        /// <summary>
        /// Post message
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public PostMessageResponse PostMessage(PostMessageArguments args)
        {
            //https://api.slack.com/methods/chat.postMessage
            String strURL = "";
            try
            {
                strURL = "https://slack.com/api/chat.postMessage?token=" + _client.APIKey + 
                    "&channel=" + System.Web.HttpUtility.UrlEncode(args.channel, Encoding.UTF8) +
                    "&text=" + System.Web.HttpUtility.UrlEncode(args.text, Encoding.UTF8) +
                    "&pretty=1";

                if (args.username.Trim().Length > 0)
                    strURL += "&username=" + System.Web.HttpUtility.UrlEncode(args.username);

                if (args.icon_url.Trim().Length > 0)
                    strURL += "&icon_url=" + System.Web.HttpUtility.UrlEncode(args.icon_url);

                if (args.icon_emoji.Trim().Length > 0)
                    strURL += "&icon_emoji=" + System.Web.HttpUtility.UrlEncode(args.icon_emoji);

                if (args.attachments != null)
                    strURL += "&attachments=" + System.Web.Helpers.Json.Encode(args.attachments);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            dynamic Response;

            try
            {
                String strResponse = _client.APIRequest(strURL);
                Response = System.Web.Helpers.Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            _client.CheckForError(Response);
            return new Slack.Chat.PostMessageResponse(_client, Response);
        }

        /// <summary>
        /// Post Ephemeral
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public PostMessageResponse PostEphemeral(PostMessageArguments args)
        {
            //https://api.slack.com/methods/chat.postEphemeral
            String strURL = "";
            try
            {
                strURL = "https://slack.com/api/chat.postEphemeral?token=" + _client.APIKey +
                    "&channel=" + System.Web.HttpUtility.UrlEncode(args.channel, Encoding.UTF8) +
                    "&text=" + System.Web.HttpUtility.UrlEncode(args.text, Encoding.UTF8) +
                    "&pretty=1";

                if (args.username.Trim().Length > 0)
                    strURL += "&user=" + System.Web.HttpUtility.UrlEncode(args.username);

                if (args.icon_url.Trim().Length > 0)
                    strURL += "&icon_url=" + System.Web.HttpUtility.UrlEncode(args.icon_url);

                if (args.icon_emoji.Trim().Length > 0)
                    strURL += "&icon_emoji=" + System.Web.HttpUtility.UrlEncode(args.icon_emoji);

                if (args.attachments != null)
                    strURL += "&attachments=" + System.Web.Helpers.Json.Encode(args.attachments);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            dynamic Response;

            try
            {
                String strResponse = _client.APIRequest(strURL);
                Response = System.Web.Helpers.Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            _client.CheckForError(Response);
            return new Slack.Chat.PostMessageResponse(_client, Response);
        }

        /// <summary>
        /// Post attachement
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public PostMessageResponse PostAttachement(PostAttachementMessageArguments args)
        {
            //https://api.slack.com/methods/chat.postMessage
            String strURL = "";
            try
            {
                strURL = "https://slack.com/api/chat.postMessage?token=" + _client.APIKey +
                    "&channel=" + System.Web.HttpUtility.UrlEncode(args.channel, Encoding.UTF8) +
                    "&text=" + System.Web.HttpUtility.UrlEncode(args.text, Encoding.UTF8) +
                    "&pretty=1";

                //"https://slack.com/api/chat.postMessage?token=" + _client.APIKey +
                //"&channel=" + args.channel +
                //"&text=" + System.Web.HttpUtility.UrlEncode(args.text, Encoding.UTF8) +
                //"&as_user=" + args.as_user.ToString() +
                ////"&parse=" + args.parse +
                //"&link_names=" + args.link_names.ToString() +
                //"&unfurl_links=" + args.unfurl_links.ToString() +
                //"&unfurl_media=" + args.unfurl_media.ToString();

                if (args.username.Trim().Length > 0)
                    strURL += "&username=" + args.username;

                if (args.icon_url.Trim().Length > 0)
                    strURL += "&icon_url=" + args.icon_url;

                if (args.icon_emoji.Trim().Length > 0)
                    strURL += "&icon_emoji=" + args.icon_emoji;

                //if (args.attachments != null)
                //    strURL += "&attachments=" + System.Web.Helpers.Json.Encode(args.attachments);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            dynamic Response;

            try
            {
                String strResponse = _client.APIPostRequest(strURL, args.attachments != null ? System.Web.Helpers.Json.Encode(args.attachments) : null);
                Response = System.Web.Helpers.Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            _client.CheckForError(Response);
            return new Slack.Chat.PostMessageResponse(_client, Response);
        }

        /// <summary>
        /// Post image
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public void PostImage(PostImageArguments args)
        {
            //https://api.slack.com/methods/files.upload
            String strURL = "";
            try
            {
                strURL =
                    "https://slack.com/api/files.upload?token=" + _client.APIKey +
                    "&channels=" + System.Web.HttpUtility.UrlEncode(args.channel) +
                    "&title=" + System.Web.HttpUtility.UrlEncode(args.title, Encoding.UTF8) +
                    "&initial_comment=" + System.Web.HttpUtility.UrlEncode(args.initial_comment, Encoding.UTF8) +
                    "&pretty=1";
            }

            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            dynamic Response;

            try
            {
                string[] file = new string[1];
                file[0] = args.file;
                String strResponse = _client.APIRequestForImage(strURL, args.title, file);
                Response = System.Web.Helpers.Json.Decode(strResponse);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not post chat message.", ex);
            }

            _client.CheckForError(Response);
            //return new Slack.Chat.PostMessageResponse(_client, Response);
        }

        /// <summary>
        /// Get post message URL
        /// </summary>
        /// <param name="client"></param>
        /// <param name="channel"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string GetPostMessageURL(Slack.Client client, string channel, string text)
        {
            string strURL = "https://slack.com/api/chat.postMessage?token=" + client.APIKey + "&channel=" + channel + "&text=" + text;
            return strURL;

            // +"&as_user=False&parse=full&link_names=1&unfurl_links=True&unfurl_media=True";
            //return System.Web.HttpUtility.UrlEncode(strURL);
        }
    }



}
