﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SlackBOT
{
    partial class Service : ServiceBase
    {
        private Timer timer;
        string module = "Slack BOT Service", method = string.Empty;

        public Service()
        {
            InitializeComponent();
            eventLog1 = new EventLog();
            eventLog1.MachineName = System.Environment.MachineName;
        }

        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            method = "OnStart";
            eventLog1.Source = method;
            eventLog1.WriteEntry("Service started.", EventLogEntryType.Information);
            timer = new Timer(StartServiceListener, null, 0, Timeout.Infinite);
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            method = "OnStop";
            eventLog1.Source = method;
            eventLog1.WriteEntry("Service stopped.", EventLogEntryType.Information);
            timer.Dispose();
        }

        private void StartServiceListener(object e)
        {
            method = "StartServiceListener";
            eventLog1.Source = method;
            eventLog1.WriteEntry("Calling service listener...", EventLogEntryType.Information);

            try
            {
                Program.StartSlackClient();
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("Start Bot Error", EventLogEntryType.Error);
                eventLog1.WriteEntry(ex.Message, EventLogEntryType.Error);
                eventLog1.WriteEntry(ex.StackTrace, EventLogEntryType.Error);
            }
        }
    }
}
