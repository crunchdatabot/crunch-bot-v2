﻿using QlikLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace SlackBOT
{
    public class Program
    {
        private static Bot qSlackBot;
        static string module = "Slack BOT Service";

        private static void StartService()
        {
            ServiceBase[] ServiceToRun;
            ServiceToRun = new ServiceBase[]
            {
                new Service()
            };
            ServiceBase.Run(ServiceToRun);
        }

        static void Main(string[] args)
        {
            if (Environment.UserInteractive == true)
                StartSlackClient();
            else
                StartService();
        }

        public static void StartSlackClient()
        {
            string method = "Main";
            LogFile log = new LogFile(ConfigurationManager.AppSettings["logfilepath"]);

            try
            {
                qSlackBot = new Bot();
            }
            catch (Exception e)
            {
                log.Error(module, method, "Start Bot Error", true);
                log.Error(module, method, e.Message);
                log.Error(module, method, e.StackTrace);
            }

            Console.ReadLine();
        }
    }
}
